<?php

    class ElectronicItem
    {
        const ELECTRONIC_ITEM_CONSOLE = 'console';
        const ELECTRONIC_ITEM_TELEVISION = 'television';
        const ELECTRONIC_ITEM_MICROWAVE = 'microwave';


        private static  $types = array(
            self::ELECTRONIC_ITEM_CONSOLE,
            self::ELECTRONIC_ITEM_TELEVISION,
            self::ELECTRONIC_ITEM_MICROWAVE
        );

        private static $maxExtras = array(
           self:: ELECTRONIC_ITEM_CONSOLE => 4,
           self:: ELECTRONIC_ITEM_MICROWAVE => 0
        );

        public function verifyType($type)
        {
            if (in_array($type, self::$types))
            {
                return true;
            }
            return false;
        }

        function maxExtras($type, $number)
        {
            if (array_key_exists($type, self::$maxExtras))
            {
                if (self::$maxExtras[$type] < $number)
                {
                    return false;
                }
            }
            return true;
        }
    }
