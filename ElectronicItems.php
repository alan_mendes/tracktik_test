<?php

    require_once("ElectronicItem.php");

    class ElectronicItems extends ElectronicItem
    {

        private $items = array();

        public function __construct(array $items)
        {
            $this->items = $items;
        }

        public function unsetTypes()
        {
            foreach ($this->items as $key => $item)
            {
                if(parent::verifyType($item['type']) == false)
                {
                    unset($this->items[$key]);
                }
            }
        }

        public function checkMaxExtras()
        {
            foreach ($this->items as $item) {

                $number = 0;
                if (array_key_exists('remote_controllers', $item))
                    $number = $item['remote_controllers'];

                if (array_key_exists('wired_controllers', $item))
                    $number +=  $item['wired_controllers'];

                if (parent::maxExtras($item['type'], $number) == false)
                {
                    exit("Max extra exceeded");
                }
            }
        }

        public function getSortedItems($key)
        {
           array_multisort(array_column($this->items, $key), SORT_ASC, $this->items);
           return $this->items;
        }

        public function getSumPrice()
        {
            $sumPrice = 0;
            foreach ($this->items as $item) {
                $sumPrice += $item['price'];
            }
            return $sumPrice;
        }

        public function getItemPrice($type)
        {
            foreach ($this->items as $item) {
                if ($item['type'] == $type)
                    return $item['price'];
            }
            return false;
        }
    }
