<?php
    require_once("ElectronicItems.php");

    $items = array(
        array(
            'type' => 'console',
            'price' => 200 ,
            'number' => 1,
            'remote_controllers' => 2,
            'wired_controllers' => 2
        ),
        array(
            'type' =>'television',
            'price' => 1000,
            'number' => 1,
            'remote_controllers' => 2
        ),
        array(
            'type' =>'television',
            'price' => 1000,
            'number' => 1,
            'remote_controllers' => 1
        ),
        array(
            'type' =>'microwave',
            'price' => 300,
            'number' => 1
        )
    );

    $electItems = new ElectronicItems($items);
    $electItems->unsetTypes();
    $electItems->checkMaxExtras();

    $new_item = $electItems->getSortedItems('price');
    $sumPrice = $electItems-> getSumPrice();
    $itemPrice = $electItems-> getItemPrice('console');

    print("<h4>Sort by price: ");
    foreach ($new_item as $item) {
        print "<ul> item:";
        foreach($item as $key=>$value)
        print "<li>" . $key ." = ". $value ."</li>";
        print "</ul>";
    }
    print "</h4>";

    print "<h4> Question 1: Total: " . $sumPrice . "</h4>";
    print "<h4> Question 2: Console and controllers: " . $itemPrice . "</h4>";

?>
